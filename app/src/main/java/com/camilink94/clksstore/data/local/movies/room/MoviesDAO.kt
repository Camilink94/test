package com.camilink94.clksstore.data.local.movies.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface MoviesDAO {

    @Query("SELECT * FROM movies")
    suspend fun getAll(): List<MovieEntity>

    @Insert
    suspend fun addAll(vararg movies: MovieEntity)

    @Query("SELECT * FROM movies where id like :id limit 1")
    suspend fun getMovieById(id: String): List<MovieEntity>

    @Query("DELETE FROM movies")
    suspend fun nukeMovies()


    @Query("SELECT * FROM cart")
    suspend fun getCartItems(): List<CartEntity>

    @Insert
    suspend fun addToCart(item: CartEntity)

    @Query("DELETE FROM cart WHERE id = :id")
    suspend fun removeFromCart(id: Int)

    @Query("DELETE FROM cart")
    suspend fun clearCart()

}