package com.camilink94.clksstore.data.base

import com.squareup.moshi.Json



open class RemoteResponseModel {
    open var error: RemoteErrorModel? = null
}

/**
 * The default error response.
 */
data class RemoteErrorModel(
    @field:Json(name = "code")
    var code: Int = -1,

    @field:Json(name = "title")
    val title: String? = null,

    @field:Json(name = "detail")
    var description: String = ""
)
