package com.camilink94.clksstore.data.local.movies.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "movies")
data class MovieEntity(
    @PrimaryKey val uid: Int?,
    @ColumnInfo(name = "adult") val adult: Boolean = false,
    @ColumnInfo(name = "backdrop_path") val backdropPath: String = "",
    @ColumnInfo(name = "id") val id: Int = 0,
    @ColumnInfo(name = "original_language") val originalLanguage: String = "",
    @ColumnInfo(name = "original_title") val originalTitle: String = "",
    @ColumnInfo(name = "overview") val overview: String = "",
    @ColumnInfo(name = "popularity") val popularity: Double = 0.0,
    @ColumnInfo(name = "poster_path") val posterPath: String = "",
    @ColumnInfo(name = "release_date") val releaseDate: String = "",
    @ColumnInfo(name = "title") val title: String = "",
    @ColumnInfo(name = "video") val video: Boolean = false,
    @ColumnInfo(name = "vote_average") val voteAverage: Double = 0.0,
    @ColumnInfo(name = "vote_count") val voteCount: Int = 0
)

@Entity(tableName = "cart")
data class CartEntity(
    @PrimaryKey val uid: Int?,

    @ColumnInfo(name = "id") val id: Int = 0,
    @ColumnInfo(name = "original_title") val originalTitle: String = "",
    @ColumnInfo(name = "overview") val overview: String = "",
    @ColumnInfo(name = "poster_path") val posterPath: String = "",
    @ColumnInfo(name = "backdrop_path") val backdropPath: String = "",

    @ColumnInfo(name = "adult") val adult: Boolean = false,
    @ColumnInfo(name = "original_language") val originalLanguage: String = "",
    @ColumnInfo(name = "popularity") val popularity: Double = 0.0,
    @ColumnInfo(name = "release_date") val releaseDate: String = "",
    @ColumnInfo(name = "title") val title: String = "",
    @ColumnInfo(name = "video") val video: Boolean = false,
    @ColumnInfo(name = "vote_average") val voteAverage: Double = 0.0,
    @ColumnInfo(name = "vote_count") val voteCount: Int = 0
)