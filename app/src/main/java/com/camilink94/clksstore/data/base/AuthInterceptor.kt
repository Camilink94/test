package com.camilink94.clksstore.data.base

import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor : Interceptor {

    val apiKey = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJkMmI3ZmQ3Njc2YjIwZGI4OTE2MWUzOWE4ZWRiMzFiNCIsInN1YiI6IjVkOGQ0YjRlMTcyZDdmMDAwZDUyNDc5MiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.oB9sPsb0xrfdV8acFlrNnjlqgXMUq8vIvQDWWwDfRWQ"

    override fun intercept(chain: Interceptor.Chain): Response {

        val newReq = chain.request().newBuilder()
        newReq.addHeader("Authorization", "Bearer $apiKey")

        return chain.proceed(newReq.build())
    }
}