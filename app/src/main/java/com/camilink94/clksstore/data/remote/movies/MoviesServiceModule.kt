package com.camilink94.clksstore.data.remote.movies

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class MoviesServiceModule {

    @Singleton
    @Binds
    abstract fun bindMoviesService(
        service: MoviesService
    ): IMoviesService
}