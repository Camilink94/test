package com.camilink94.clksstore.data.local.movies

import com.camilink94.clksstore.data.local.movies.room.CartEntity
import com.camilink94.clksstore.data.local.movies.room.MovieEntity
import com.camilink94.clksstore.ui.list.MovieItem

interface ILocalMoviesDB {
    suspend fun getAllMovies(): List<MovieEntity>
    suspend fun saveAllMovies(movies: List<MovieEntity>, force: Boolean = false)
    suspend fun getMovieById(id: String): List<MovieEntity>
    suspend fun nukeMovies()

    suspend fun getCartItems(): List<CartEntity>
    suspend fun addToCart(item: CartEntity)
    suspend fun removeFromCart(item: MovieItem)
    suspend fun clearCart()
}