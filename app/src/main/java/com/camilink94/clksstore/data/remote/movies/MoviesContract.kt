package com.camilink94.clksstore.data.remote.movies

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface MoviesContract {

    @GET("movie/popular/")
    suspend fun getPopularMovies(): Response<PopularMovieRemoteResponse>

    @GET("movie/{movie_id}")
    suspend fun getMovieDetails(@Path("movie_id") id: String): Response<MovieDetailsRemoteResponse>
}