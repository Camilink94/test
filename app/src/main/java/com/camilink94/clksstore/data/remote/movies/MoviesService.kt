package com.camilink94.clksstore.data.remote.movies

import com.camilink94.clksstore.data.base.BaseRetrofitService
import com.camilink94.clksstore.data.base.RemoteErrorModel
import javax.inject.Inject

class MoviesService @Inject constructor() : BaseRetrofitService(), IMoviesService {

    private val contract: MoviesContract = build(MoviesContract::class.java)

    override suspend fun getPopular(): PopularMovieRemoteResponse {
        var res = PopularMovieRemoteResponse()

        try {
            val moviesRes = contract.getPopularMovies()
            if (moviesRes.isSuccessful) {
                res = moviesRes.body()!!
            } else {
                res.error =
                    RemoteErrorModel(
                        code = moviesRes.code(),
                        title = "Error",
                        description = "Ocurrió un error"
                    )
            }
        } catch (ex: Exception) {
            res.error =
                RemoteErrorModel(code = 1, title = "Error", description = "Ocurrió un error")
        }

        return res
    }

    override suspend fun getMovieDetails(id: String): MovieDetailsRemoteResponse {
        var res = MovieDetailsRemoteResponse()

        try {
            val moviesRes = contract.getMovieDetails(id)
            if (moviesRes.isSuccessful) {
                res = moviesRes.body()!!
            } else {
                res.error =
                    RemoteErrorModel(
                        code = moviesRes.code(),
                        title = "Error",
                        description = "Ocurrió un error"
                    )
            }
        } catch (ex: Exception) {
            res.error =
                RemoteErrorModel(code = 2, title = "Error", description = "Ocurrió un error")
        }

        return res
    }

}