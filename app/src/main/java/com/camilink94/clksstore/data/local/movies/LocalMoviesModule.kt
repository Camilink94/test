package com.camilink94.clksstore.data.local.movies

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class LocalMoviesModule {

    @Singleton
    @Binds
    abstract fun bindLocalMovies(
        moviesRepo: LocalMoviesDB
    ): ILocalMoviesDB
}