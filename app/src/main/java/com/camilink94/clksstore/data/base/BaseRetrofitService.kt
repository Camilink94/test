package com.camilink94.clksstore.data.base

import com.camilink94.clksstore.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.KoinComponent
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit


/**
 * Base class for any Retrofit implementation. It centralizes the Http client, and has a method
 * to create the Retrofit implementation. Subclasses should also implement an IEntryPoint.
 */
abstract class BaseRetrofitService : KoinComponent {

    private val baseUrl = "https://api.themoviedb.org/3/"

    protected fun <ContractType> build(
        ctClass: Class<ContractType>,
        secs: Int = 30
    ): ContractType {
        val retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create())
            .client(provideDefaultOkhttpClient(secs))
            .build()
        return retrofit.create(ctClass)
    }

    private fun provideDefaultOkhttpClient(secs: Int): OkHttpClient {

        val interceptor = HttpLoggingInterceptor().apply {
            level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        }
        return OkHttpClient.Builder()
            .readTimeout(secs.toLong(), TimeUnit.SECONDS)
            .writeTimeout(secs.toLong(), TimeUnit.SECONDS)
            .addInterceptor(AuthInterceptor())
            .addInterceptor(interceptor)
            .build()
    }
}