package com.camilink94.clksstore.data.remote.movies

interface IMoviesService {

    suspend fun getPopular(): PopularMovieRemoteResponse
    suspend fun getMovieDetails(id: String): MovieDetailsRemoteResponse
}