package com.camilink94.clksstore.data.local.movies

import androidx.room.Room
import com.camilink94.clksstore.data.local.movies.room.CartEntity
import com.camilink94.clksstore.data.local.movies.room.MovieEntity
import com.camilink94.clksstore.data.local.movies.room.MoviesDatabase
import com.camilink94.clksstore.ui.app.BaseApp
import com.camilink94.clksstore.ui.list.MovieItem
import javax.inject.Inject

class LocalMoviesDB @Inject constructor() : ILocalMoviesDB {

    private val roomDatabase =
        Room.databaseBuilder(BaseApp.instance, MoviesDatabase::class.java, "moviesDB").build()

    override suspend fun getAllMovies(): List<MovieEntity> {
        return roomDatabase.moviesDao().getAll()
    }

    override suspend fun saveAllMovies(movies: List<MovieEntity>, force: Boolean) {
        roomDatabase.moviesDao().addAll(movies = movies.toTypedArray())
    }

    override suspend fun getMovieById(id: String): List<MovieEntity> {
        return roomDatabase.moviesDao().getMovieById(id)
    }

    override suspend fun nukeMovies() {
        roomDatabase.moviesDao().nukeMovies()
    }


    override suspend fun getCartItems(): List<CartEntity> {
        return roomDatabase.moviesDao().getCartItems()
    }

    override suspend fun addToCart(item: CartEntity) {
        roomDatabase.moviesDao().addToCart(item)
    }

    override suspend fun removeFromCart(item: MovieItem) {
        roomDatabase.moviesDao().removeFromCart(item.id)
    }

    override suspend fun clearCart() {
        roomDatabase.moviesDao().clearCart()
    }

}