package com.camilink94.clksstore.repo.movies

import android.content.Context
import com.camilink94.clksstore.data.local.movies.ILocalMoviesDB
import com.camilink94.clksstore.data.local.movies.room.MovieEntity
import com.camilink94.clksstore.data.remote.movies.IMoviesService
import com.camilink94.clksstore.data.remote.movies.MovieDetailsRemoteResponse
import com.camilink94.clksstore.data.remote.movies.PopularMovieRemoteResponse
import com.camilink94.clksstore.ui.base.BaseUIData
import com.camilink94.clksstore.ui.details.MovieDetailsUIData
import com.camilink94.clksstore.ui.list.ListUIData
import com.camilink94.clksstore.ui.list.MovieItem
import dagger.hilt.EntryPoint
import dagger.hilt.InstallIn
import dagger.hilt.android.EntryPointAccessors
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MoviesRepo @Inject constructor(@ApplicationContext val context: Context) : IMoviesRepo {

    @InstallIn(SingletonComponent::class)
    @EntryPoint
    interface MovieServiceEntryPoint {
        fun getService(): IMoviesService
        fun getLocal(): ILocalMoviesDB
    }

    private val hiltEntryPoint =
        EntryPointAccessors.fromApplication(context, MovieServiceEntryPoint::class.java)

    private val service: IMoviesService = hiltEntryPoint.getService()
    private val db: ILocalMoviesDB = hiltEntryPoint.getLocal()


    override suspend fun getMovies(): Flow<ListUIData> = withContext(Dispatchers.IO) {
        return@withContext flow {
            val initialDbData = db.getAllMovies()

            emit(ListUIData(items = initialDbData.map { mapEntityToUI(it) }))

            val rem = service.getPopular()

            if (rem.error == null) {
                // First clear all movies, to add the new items. While this is not the most optimal solution,
                // it is a viable solution for this scope. A more optimal solution would check if the item
                // exists in the DB, and add it only if it's not in.
                db.nukeMovies()
                db.saveAllMovies(rem.results.map { mapRemoteToEntity(it) })
                emit(ListUIData(rem.results.map { mapRemoteToUI(it) }))
            } else {
                emit(ListUIData().apply { error = BaseUIData.getFromRemote(rem.error) })
            }
        }
    }

    private fun mapEntityToUI(entity: MovieEntity): MovieItem {
        return MovieItem(
            id = entity.id,
            name = entity.originalTitle,
            desc = entity.overview,
            posterUrl = "https://image.tmdb.org/t/p/w500/${entity.posterPath}",
            bgUrl = "https://image.tmdb.org/t/p/w500/${entity.backdropPath}"
        )
    }

    private fun mapRemoteToUI(remote: PopularMovieRemoteResponse.SingleMovieData): MovieItem {
        return MovieItem(
            id = remote.id,
            name = remote.originalTitle,
            desc = remote.overview,
            posterUrl = "https://image.tmdb.org/t/p/w500/${remote.posterPath}",
            bgUrl = "https://image.tmdb.org/t/p/w500/${remote.backdropPath}"
        )
    }

    private fun mapRemoteToEntity(remote: PopularMovieRemoteResponse.SingleMovieData): MovieEntity {

        return remote.run {
            MovieEntity(
                null,
                adult,
                backdropPath,
                id,
                originalLanguage,
                originalTitle,
                overview,
                popularity,
                posterPath,
                releaseDate,
                title,
                video,
                voteAverage,
                voteCount
            )
        }
    }

    override suspend fun getMovieInfo(id: String): MovieDetailsUIData =
        withContext(Dispatchers.IO) {
            var res = MovieDetailsUIData()

            val local = db.getMovieById(id)
            if (local.isEmpty()) {
                val rem = service.getMovieDetails(id)
                if (rem.error == null) {
                    res = MovieDetailsUIData(item = mapRemoteDetailsToUI(rem))
                } else {
                    res.error = BaseUIData.getFromRemote(rem.error)
                }
            } else {
                res = MovieDetailsUIData(item = mapEntityToUI(local[0]))
            }

            return@withContext res
        }

    private fun mapRemoteDetailsToUI(remote: MovieDetailsRemoteResponse): MovieItem {
        return MovieItem(
            id = remote.id,
            name = remote.originalTitle,
            desc = remote.overview,
            posterUrl = "https://image.tmdb.org/t/p/w500/${remote.posterPath}",
            bgUrl = "https://image.tmdb.org/t/p/w500/${remote.backdropPath}"
        )
    }

}