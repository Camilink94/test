package com.camilink94.clksstore.repo.cart

import com.camilink94.clksstore.ui.cart.CartUIData
import com.camilink94.clksstore.ui.list.MovieItem

interface ICartRepo {
    suspend fun getCartItems(): CartUIData

    suspend fun addToCart(item: MovieItem)
    suspend fun removeFromCart(item: MovieItem)

    suspend fun clearCart()
}