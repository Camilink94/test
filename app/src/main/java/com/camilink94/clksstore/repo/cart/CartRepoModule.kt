package com.camilink94.clksstore.repo.cart

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class CartRepoModule {

    @Binds
    abstract fun bindCartRepo(
        moviesRepo: CartRepo
    ): ICartRepo
}