package com.camilink94.clksstore.repo.cart

import android.content.Context
import com.camilink94.clksstore.data.local.movies.ILocalMoviesDB
import com.camilink94.clksstore.data.local.movies.room.CartEntity
import com.camilink94.clksstore.ui.cart.CartUIData
import com.camilink94.clksstore.ui.list.MovieItem
import dagger.hilt.EntryPoint
import dagger.hilt.InstallIn
import dagger.hilt.android.EntryPointAccessors
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class CartRepo @Inject constructor(@ApplicationContext val context: Context) : ICartRepo {

    @InstallIn(SingletonComponent::class)
    @EntryPoint
    interface MovieServiceEntryPoint {
        fun getLocal(): ILocalMoviesDB
    }

    private val db: ILocalMoviesDB = run {
        val hiltEntryPoint =
            EntryPointAccessors.fromApplication(context, MovieServiceEntryPoint::class.java)
        hiltEntryPoint.getLocal()
    }

    override suspend fun getCartItems(): CartUIData = withContext(Dispatchers.IO) {
        var res = CartUIData()

        val rem = db.getCartItems()
        res = CartUIData(items = rem.map {
            MovieItem(
                id = it.id,
                name = it.originalTitle, desc = it.overview,
                posterUrl = it.posterPath,
                bgUrl = it.backdropPath
            )
        })

        return@withContext res
    }

    override suspend fun addToCart(item: MovieItem) = withContext(Dispatchers.IO) {
        db.addToCart(
            item = CartEntity(
                uid = null,
                id = item.id,
                originalTitle = item.name,
                overview = item.desc,
                posterPath = item.posterUrl,
                backdropPath = item.bgUrl
            )
        )
    }

    override suspend fun removeFromCart(item: MovieItem) = withContext(Dispatchers.IO) {
        db.removeFromCart(item)
    }

    override suspend fun clearCart() = withContext(Dispatchers.IO) {
        db.clearCart()
    }
}