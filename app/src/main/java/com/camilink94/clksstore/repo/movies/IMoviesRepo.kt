package com.camilink94.clksstore.repo.movies

import com.camilink94.clksstore.ui.details.MovieDetailsUIData
import com.camilink94.clksstore.ui.list.ListUIData
import kotlinx.coroutines.flow.Flow

interface IMoviesRepo {
    suspend fun getMovies(): Flow<ListUIData>

    suspend fun getMovieInfo(id: String): MovieDetailsUIData
}