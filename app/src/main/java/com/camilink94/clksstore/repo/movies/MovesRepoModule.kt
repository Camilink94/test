package com.camilink94.clksstore.repo.movies

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class MovesRepoModule {

    @Binds
    abstract fun bindMoviesRepo(
        moviesRepo: MoviesRepo
    ): IMoviesRepo
}