package com.camilink94.clksstore.ui.cart

import com.camilink94.clksstore.ui.base.BaseUIData
import com.camilink94.clksstore.ui.list.MovieItem

data class CartUIData(
    val items: List<MovieItem> = listOf()
) : BaseUIData()