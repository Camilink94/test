package com.camilink94.clksstore.ui.list

import com.camilink94.clksstore.ui.base.BaseUIData

data class ListUIData(
    val items: List<MovieItem> = listOf()
) : BaseUIData()

data class MovieItem(
    val id: Int = 0,
    val name: String = "", val desc: String = "",
    val posterUrl: String = "", val bgUrl: String = "",

    var added: Boolean = false
)