package com.camilink94.clksstore.ui.base

import com.camilink94.clksstore.data.base.RemoteErrorModel

open class BaseUIData {
    var error: UIDataError? = null

    companion object {
        fun getFromRemote(remote: RemoteErrorModel?): UIDataError {
            return remote?.let {
                UIDataError(
                    id = remote.code,
                    title = remote.title ?: "",
                    message = remote.description
                )
            } ?: UIDataError()
        }
    }
}

data class UIDataError(
    val id: Int = 0, val title: String = "", val message: String = ""
)