package com.camilink94.clksstore.ui.list

import android.os.Bundle
import android.view.*
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.camilink94.clksstore.R
import com.camilink94.clksstore.databinding.FragmentListBinding
import com.camilink94.clksstore.databinding.ItemMovieBinding
import com.camilink94.clksstore.ui.base.BaseFragment
import com.camilink94.clksstore.ui.extentions.loadUrl
import com.camilink94.clksstore.ui.extentions.navigateWithDefaultAnimation
import com.camilink94.clksstore.ui.shared.SharedViewModel
import dagger.hilt.android.AndroidEntryPoint
import me.ibrahimyilmaz.kiel.adapterOf
import me.ibrahimyilmaz.kiel.core.RecyclerViewHolder

@AndroidEntryPoint
class ListFragment : BaseFragment() {

    lateinit var binding: FragmentListBinding

    private val vm: ListViewModel by viewModels()
    private val svm by activityViewModels<SharedViewModel>()

    private val adapter = adapterOf<MovieItem> {
        register(
            viewHolder = ::MovieItemViewHolder,
            layoutResource = R.layout.item_movie,
            onBindViewHolder = { vh, pos, item ->
                vh.bind(item)
            })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentListBinding.inflate(inflater, container, false)

        binding.items.adapter = adapter

        binding.swipe.setOnRefreshListener {
            vm.getItems()
        }

        vm.items.observe(viewLifecycleOwner) {
            it?.let {
                if (it.error == null) {
                    adapter.submitList(it.items)
                }
            }
        }

        vm.loading.observe(viewLifecycleOwner) {
            it?.let {
                binding.swipe.isRefreshing = it
            }
        }

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_cart -> {
                findNavController().navigateWithDefaultAnimation(R.id.nav_cart)
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    fun onItemClicked(item: MovieItem) {
        svm.selectMovie(item)
        findNavController().navigateWithDefaultAnimation(R.id.nav_details)
    }

    fun onAddItemClicked(item: MovieItem) {
        vm.addToCart(item)
    }

    fun onRemoveItemClicked(item: MovieItem) {
        vm.removeFromCart(item)
    }

    inner class MovieItemViewHolder(view: View) : RecyclerViewHolder<MovieItem>(view) {
        val binding = ItemMovieBinding.bind(view)

        var item: MovieItem? = null

        init {
            binding.run {
                container.setOnClickListener {
                    this@MovieItemViewHolder.item?.let {
                        onItemClicked(it)
                    }
                }

                addBtn.setOnClickListener {
                    this@MovieItemViewHolder.item?.let {
                        onAddItemClicked(it)
                    }
                }
                removeBtn.setOnClickListener {
                    this@MovieItemViewHolder.item?.let {
                        onRemoveItemClicked(it)
                    }

                }
            }
        }

        fun bind(item: MovieItem) {
            this.item = item
            binding.run {
                title.text = item.name
                desc.text = item.desc
                background.loadUrl(item.bgUrl)
                poster.loadUrl(item.posterUrl)

                if (item.added) {
                    addBtn.visibility = View.GONE
                    removeBtn.visibility = View.VISIBLE
                } else {
                    addBtn.visibility = View.VISIBLE
                    removeBtn.visibility = View.GONE
                }
            }

        }
    }
}