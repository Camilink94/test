package com.camilink94.clksstore.ui.extentions

import android.os.Bundle
import android.widget.ImageView
import androidx.annotation.IdRes
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import com.camilink94.clksstore.R
import com.squareup.picasso.Picasso

fun ImageView.loadUrl(url: String) {
    try {
        Picasso.get().load(url).into(this)
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
}

fun NavController.navigateWithDefaultAnimation(
    @IdRes destId: Int,
    @IdRes popUpTo: Int? = null,
    popUpInclusive: Boolean = false,
    args: Bundle? = null
): Boolean {
    return try {
        val navOptions = getNavOptions(popUpTo, popUpInclusive)

        navigate(destId, args, navOptions)
        true
    } catch (ex: Exception) {
        ex.printStackTrace()
        false
    }
}

private fun getNavOptions(popUpTo: Int?, popUpInclusive: Boolean): NavOptions {
    val navOptionsBuilder = getDefaultAnimNavOptionsBuilder()

    if (popUpTo != null)
        navOptionsBuilder.setPopUpTo(popUpTo, popUpInclusive)

    return navOptionsBuilder.build()
}

fun getDefaultAnimNavOptionsBuilder(): NavOptions.Builder {
    return NavOptions.Builder()
        .setEnterAnim(R.anim.nav_default_enter_anim)
        .setExitAnim(R.anim.nav_default_exit_anim)
        .setPopEnterAnim(R.anim.nav_default_pop_enter_anim)
        .setPopExitAnim(R.anim.nav_default_pop_exit_anim)
}