package com.camilink94.clksstore.ui.cart

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.camilink94.clksstore.repo.cart.ICartRepo
import com.camilink94.clksstore.ui.list.MovieItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CartViewModel @Inject constructor(
    private val repo: ICartRepo
) : ViewModel() {

    val loading = MutableLiveData(false)
    val cartData: MutableLiveData<CartUIData> = MutableLiveData()

    init {
        getCartItems()
    }

    fun getCartItems() {
        viewModelScope.launch {
            loading.value = true

            val rem = repo.getCartItems()
            cartData.value = rem

            loading.value = false
        }
    }

    fun removeFromCart(item: MovieItem) {
        viewModelScope.launch {
            loading.value = true

            repo.removeFromCart(item)

            val rem = repo.getCartItems()
            cartData.value = rem

            loading.value = false
        }
    }

    fun clearCart() {
        viewModelScope.launch {
            loading.value = true

            repo.clearCart()

            val rem = repo.getCartItems()
            cartData.value = rem

            loading.value = false
        }
    }

}