package com.camilink94.clksstore.ui.details

import android.os.Bundle
import android.view.*
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.camilink94.clksstore.R
import com.camilink94.clksstore.databinding.FragmentDetailsBinding
import com.camilink94.clksstore.ui.base.BaseFragment
import com.camilink94.clksstore.ui.extentions.loadUrl
import com.camilink94.clksstore.ui.extentions.navigateWithDefaultAnimation
import com.camilink94.clksstore.ui.shared.SharedViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailsFragment : BaseFragment() {

    lateinit var binding: FragmentDetailsBinding

    private val svm by activityViewModels<SharedViewModel>()

    private val navArgs by navArgs<DetailsFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailsBinding.inflate(inflater, container, false)

        binding.run {
            addBtn.setOnClickListener { svm.addToCart() }
            removeBtn.setOnClickListener { svm.removeFromCart() }
        }

        svm.loading.observe(viewLifecycleOwner) {
            it?.let {
                if (it) {
                } else {
                }
            }
        }

        svm.selectedMovie.observe(viewLifecycleOwner) {
            it?.let {
                binding.run {
                    title.text = it.item.name
                    desc.text = it.item.desc
                    poster.loadUrl(it.item.posterUrl)
                    background.loadUrl(it.item.bgUrl)

                    if (it.item.added) {
                        addBtn.visibility = View.GONE
                        removeBtn.visibility = View.VISIBLE
                    } else {
                        addBtn.visibility = View.VISIBLE
                        removeBtn.visibility = View.GONE
                    }
                }
            }
        }

        if (navArgs.id != "") {
            svm.getMovieInfo(navArgs.id)
        }

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_list, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_cart -> {
                findNavController().navigateWithDefaultAnimation(R.id.nav_cart)
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }
}