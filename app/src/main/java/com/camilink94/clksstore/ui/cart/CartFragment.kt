package com.camilink94.clksstore.ui.cart

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.camilink94.clksstore.R
import com.camilink94.clksstore.databinding.FragmentCartBinding
import com.camilink94.clksstore.databinding.ItemCartBinding
import com.camilink94.clksstore.ui.base.BaseFragment
import com.camilink94.clksstore.ui.extentions.loadUrl
import com.camilink94.clksstore.ui.list.MovieItem
import dagger.hilt.android.AndroidEntryPoint
import me.ibrahimyilmaz.kiel.adapterOf
import me.ibrahimyilmaz.kiel.core.RecyclerViewHolder

@AndroidEntryPoint
class CartFragment : BaseFragment() {

    lateinit var binding: FragmentCartBinding

    private val vm by viewModels<CartViewModel>()

    private val adapter = adapterOf<MovieItem> {
        register(
            viewHolder = ::MovieItemViewHolder,
            layoutResource = R.layout.item_cart,
            onBindViewHolder = { vh, pos, item ->
                vh.bind(item)
            })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCartBinding.inflate(inflater, container, false)

        binding.items.adapter = adapter

        binding.deleteAllBtn.setOnClickListener {
            AlertDialog.Builder(requireContext())
                .setTitle("Limpiar carrito")
                .setMessage("¿Seguro que deseas limpiar el carrito?")
                .setPositiveButton("Limpiar") { _, _ -> vm.clearCart() }
                .setNegativeButton("Cancelar", null)
                .show()
        }

        vm.cartData.observe(viewLifecycleOwner) {
            it?.let {
                if (it.error == null) {
                    adapter.submitList(it.items)
                } else {
                }
            }
        }

        return binding.root
    }

    fun onRemoveItemClicked(item: MovieItem) {
        vm.removeFromCart(item)
    }

    inner class MovieItemViewHolder(view: View) : RecyclerViewHolder<MovieItem>(view) {
        val binding = ItemCartBinding.bind(view)

        var item: MovieItem? = null

        init {
            binding.run {
                removeBtn.setOnClickListener {
                    this@MovieItemViewHolder.item?.let {
                        onRemoveItemClicked(it)
                    }
                }
            }
        }

        fun bind(item: MovieItem) {
            this.item = item
            binding.run {
                title.text = item.name
                desc.text = item.desc
                background.loadUrl(item.bgUrl)
                poster.loadUrl(item.posterUrl)
            }

        }
    }

}