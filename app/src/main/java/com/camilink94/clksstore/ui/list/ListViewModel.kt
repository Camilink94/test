package com.camilink94.clksstore.ui.list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.camilink94.clksstore.repo.cart.ICartRepo
import com.camilink94.clksstore.repo.movies.IMoviesRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ListViewModel @Inject constructor(
    private val repo: IMoviesRepo,
    private val cartRepo: ICartRepo
) : ViewModel() {

    val loading = MutableLiveData(false)
    val items: MutableLiveData<ListUIData> = MutableLiveData()

    init {
        getItems()
    }

    fun getItems() {
        viewModelScope.launch {
            loading.value = true

            val cartItems = cartRepo.getCartItems()

            val remoteItemsFlow = repo.getMovies()
            remoteItemsFlow.collect { it ->
                items.value = it.apply {
                    if (error == null) items.forEach { remoteItem ->
                        remoteItem.added = cartItems.items.any { it.id == remoteItem.id }
                    }
                }
            }

            loading.value = false
        }
    }

    fun addToCart(item: MovieItem) {
        viewModelScope.launch {
            cartRepo.addToCart(item)

            getItems()
        }
    }

    fun removeFromCart(item: MovieItem) {
        viewModelScope.launch {
            loading.value = true

            cartRepo.removeFromCart(item)

            getItems()

            loading.value = false
        }
    }

}