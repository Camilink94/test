package com.camilink94.clksstore.ui.shared

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.camilink94.clksstore.repo.cart.ICartRepo
import com.camilink94.clksstore.repo.movies.IMoviesRepo
import com.camilink94.clksstore.ui.details.MovieDetailsUIData
import com.camilink94.clksstore.ui.list.MovieItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SharedViewModel @Inject constructor(
    private val moviesRepo: IMoviesRepo,
    private val cartRepo: ICartRepo
) : ViewModel() {

    val selectedMovie: MutableLiveData<MovieDetailsUIData> = MutableLiveData()
    val loading = MutableLiveData(false)

    fun getMovieInfo(id: String) {
        viewModelScope.launch {
            loading.value = true

            val rem = moviesRepo.getMovieInfo(id)

            val cartItems = cartRepo.getCartItems()

            selectedMovie.value = rem.apply {
                this.item.added = cartItems.items.any { it.id == item.id }
            }

            loading.value = false
        }
    }

    fun selectMovie(item: MovieItem) {
        selectedMovie.value = MovieDetailsUIData(item)
    }

    fun addToCart() {
        viewModelScope.launch {
            selectedMovie.value?.let {
                cartRepo.addToCart(it.item)

                val cartItems = cartRepo.getCartItems()
                selectedMovie.value = it.apply {
                    this.item.added = cartItems.items.any { it.id == item.id }
                }
            }

        }
    }

    fun removeFromCart() {
        viewModelScope.launch {
            selectedMovie.value?.let {
                cartRepo.removeFromCart(it.item)

                val cartItems = cartRepo.getCartItems()
                selectedMovie.value = it.apply {
                    this.item.added = cartItems.items.any { it.id == item.id }
                }
            }
        }
    }
}