package com.camilink94.clksstore.ui.details

import com.camilink94.clksstore.ui.base.BaseUIData
import com.camilink94.clksstore.ui.list.MovieItem

data class MovieDetailsUIData(
    val item: MovieItem = MovieItem()
) : BaseUIData()