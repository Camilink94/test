package com.camilink94.clksstore

import com.camilink94.clksstore.data.remote.movies.MoviesService
import kotlinx.coroutines.runBlocking
import org.junit.Test

class ServiceTest {

    @Test
    fun testListService() {
        val service = MoviesService()
        runBlocking {
            val res = service.getPopular()
            assert(res.error == null)
            assert(res.results.isNotEmpty())
        }
    }

    @Test
    fun testDetailsService() {
        val service = MoviesService()
        runBlocking {
            val res = service.getMovieDetails("550")
            assert(res.error == null)
        }
    }

}