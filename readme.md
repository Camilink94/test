# Prueba técnica de ingreso a Merqueo
#### Presentada por Camilo Chaparro
# Requerimientos técnicos para construir el proyecto:
- Android Studio 4.1+
- Kotlin Plugin 1.4.32
- Android Build Tools 1.4.32
- Dispositivo / Emulador con Android 5.0 o más nuevo (API 21)

# Deep Link

El deeplink usado en esta aplicación usa la url https://clkstore.com/details/{id}, donde {id} es el id de la película a consultar en TMDB. Para probarlo se pueden usar los comandos
- Para ver una película consultada en el servicio principal (populares):
> am start -W -a android.intent.action.VIEW -d "https://clkstore.com/details/399566" com.camilink94.clksstore
- Para ver una película no incluída:
> am start -W -a android.intent.action.VIEW -d "https://clkstore.com/details/550" com.camilink94.clksstore

# Capas propuestas
La aplicación usa la arquitectura MVVM, y consiste de 3 capas principales:
- UI
- Repositorio
- Data

### UI
En esta capa están los componentes de vista de Android, así como los ViewModels y los distintos modelos de UI
### Data
Aquí están los distintos accesos a datos, separados por paquetes. En este caso son sólo el local (DB) y el remoto (API). De igual manera, se encuentran los modelos usados por cada acceso a datos, así como las interfaces que definen cada acceso a datos, y los módulos de DI.
## Repo
En esta capa se encuentran los dos repositorios usados, cuya funcionalidad es usar los accesos a datos, poner el proceso en el hilo correcto (Dispatchers.IO, en este caso ya que se usa corrutinas), y mapear los datos del modelo de la capa Data a los modelos de la capa UI. De igual manera, también están los módulos de DI.


# Screenshots
## Lista
![lista](https://i.ibb.co/djmgxBw/Screenshot-1617647501.png)

## Detalles
![detalles](https://i.ibb.co/Y2pQLZT/Screenshot-1617647769.png)

## Carrito
![carrito](https://i.ibb.co/K7qM3Pq/Screenshot-1617647512.png)